import React, { Component } from 'react';
import { Router, Route, IndexRoute, IndexRedirect } from 'react-router'; // 路由

import { DIST } from 'conf';

// App为入口
import App from './pages/App';

// 功能模块
import StyleTpl from './pages/left/styleTpl/Index';
import CompleteTpl from './pages/left/completeTpl/Index';
import ImgSource from './pages/left/imgSource/Index';
import MyArticle from './pages/left/myArticle/Index';

// 后台系统
import Admin from './pages/admin/Index.jsx';
import AdminUser from './pages/admin/user/Index';
import AdminImgs from './pages/admin/imgs/Index';
import AdminTpls from './pages/admin/tpls/Index';
import AdminRepassword from './pages/admin/repassword/Index';
import AdminArticle from './pages/admin/article/Index';

// redux案例展示
import ReduxDom from './pages/reduxTest/ReduxDom';

// 404
import NotFound from './pages/common/NotFound';

class Routers extends Component {
    leavePath() {
        // ...
    }
    enterPath() {
        document.body.scrollTop = 0;
    }
    render() {
        return (
            <Router history={this.props.history}>
                <Route path={DIST + '/'} component={App}>
                    <IndexRedirect to={DIST + '/styletpl'} />
                    <Route onEnter={this.enterPath} path={DIST + '/styletpl'} component={StyleTpl} />
                    <Route onEnter={this.enterPath} path={DIST + '/completetpl'} component={CompleteTpl} />
                    <Route onEnter={this.enterPath} path={DIST + '/imgsource'} component={ImgSource} />
                    <Route onEnter={this.enterPath} path={DIST + '/myarticle'} component={MyArticle} />
                    <Route onEnter={this.enterPath} path={DIST + '/reduxdom'} component={ReduxDom} />
                    <Route onEnter={this.enterPath} path={DIST + '/admin'} component={Admin}>
                        <IndexRedirect to={DIST + '/admin/user'} />
                        <Route onEnter={this.enterPath} path={DIST + '/admin/user'} component={AdminUser} />
                        <Route onEnter={this.enterPath} path={DIST + '/admin/imgs'} component={AdminImgs} />
                        <Route onEnter={this.enterPath} path={DIST + '/admin/tpls'} component={AdminTpls} />
                        <Route onEnter={this.enterPath} path={DIST + '/admin/repassword'} component={AdminRepassword} />
                        <Route onEnter={this.enterPath} path={DIST + '/admin/article'} component={AdminArticle} />
                    </Route>
                    <Route onEnter={this.enterPath} path={DIST + '/*'} component={NotFound} />
                </Route>
            </Router>
        );
    }
}

export default Routers;