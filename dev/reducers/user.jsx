// 定义方法名称，取个名称，action 内使用一个字符串类型的 type 字段来表示将要执行的动作
export const SET_USER_INFO = 'SET_USER_INFO' // 设置用户信息

// 初始化数据
const initialState = {
    name: '', // 用户名称
    photo: '', // 用户头像
    tips: 2 // 提示信息
};

// reducer
export default function todo(state = initialState, action) {
    // console.log(action.type);
    switch (action.type) {
        case SET_USER_INFO:
            return { ...action.data };
        default: return state;
    }
}

// action 设置公司名称
export function setUserInfo(data) {
    return {
        type: SET_USER_INFO,
        data: data
    }
}
