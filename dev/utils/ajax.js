import { Tip } from 'mtui';
import { clearLocal } from './storage';

/**
 * @desc 对ajax进行进一步的封装
 * @param set 继承ajax的 set, type 默认是get, tips提示信息
 * @return promise 对象
*/
export default function ajax(set) {
    return new Promise((resolve, reject) => {
        if (set.type) {
            set.type = 'post';
        }
        $.ajax(set).done(res => {
            if (res.success) {
                if (set.tips) {
                    Tip.success(set.tips);
                }
                resolve(res);
            } else {
                if(res.code === 403) {
                    // 没有权限
                    clearLocal();
                    // location.href = '/';
                } 
                if (set.tips) {
                    Tip.error(set.tips);
                }
                reject(res);
            }
        });
    });
}