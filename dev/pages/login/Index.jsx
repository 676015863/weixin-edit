import './style.scss';
import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { Modal, Tabs } from 'mtui';
import LoginBox from './Login';

const TabItem = Tabs.TabItem;
export default class Login extends Component {
    // 构造函数
    constructor(props) {
        super(props);
        this.modalDom = null;
    }

    componentDidMount() {
        $(document).on('showLogin', e => {
            this.modalDom.showModal(true);
        });

        $(document).on('hideLogin', e => {
            this.modalDom.showModal(false);
        });
    }

    componentWillUnmount() {
        $(document).off('showLogin');
        $(document).off('hideLogin');
    }

    render() {
        return (
            <Modal className="login-modal" ref={e => { this.modalDom = e; }} style={{ height: 400, width: 360 }}>
                <Tabs type="top">
                    <TabItem name='登录'><LoginBox type="login"/></TabItem>
                    <TabItem name='注册'><LoginBox type="register"/></TabItem>
                </Tabs>
            </Modal>
        );
    }
}