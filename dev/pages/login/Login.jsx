import './style.scss';
import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { Input, Button, Tip } from 'mtui';
import { setLocal } from '../../utils/storage';
import { login, register } from '../../server/user';

export default class Login extends Component {
    // 构造函数
    constructor(props) {
        super(props);
        this.state = {
            tel: '',
            password: '',
            code: '',
            repassword: '',
            codeVer: +new Date()
        };
        this.modalDom = null;
    }

    loginHand = (e) => {
        let { tel, password, code } = this.state;
        login({ tel, password, code }).then(res => {
            if (res.success) {
                setLocal('user', res.data);
                // browserHistory.push('/');
                $(document).trigger('hideLogin');
                $(document).trigger('login', true);
            } else {
                Tip.error(res.msg);
            }
        });
    }

    registerHand = (e) => {
        let { tel, password, code } = this.state;
        register({ tel, password, code }).then(res => {
            if (res.success) {
                Tip.success(res.msg + '！去登录');
            } else {
                Tip.error(res.msg);
            }
        });
    }

    onChanges = (e, key) => {
        let obj = {};
        obj[key] = e.target.value;
        this.setState(obj);
    }

    // 变化验证码
    changeCode = () => {
        this.setState({
            codeVer: +new Date()
        });
    }

    render() {
        let { type } = this.props;
        let { password, repassword, tel, code, codeVer } = this.state;
        return (
            <div className="login" >
                <div className="loginbox">
                    {/* <h1>用户登录</h1> */}
                    <Input value={tel} onChange={e => { this.onChanges(e, 'tel'); }} block={true} size="lg" prefix={<i className="iconfont icon-user"></i>} placeholder="电话号码" />
                    <Input type="password" value={password} onChange={e => { this.onChanges(e, 'password'); }} block={true} size="lg" prefix={<i className="iconfont icon-password"></i>} placeholder="密码" />
                    {type === 'register' ?
                        <Input type="repassword" value={repassword} onChange={e => { this.onChanges(e, 'repassword'); }} block={true} size="lg" prefix={<i className="iconfont icon-password"></i>} placeholder="重复密码" /> :
                        null
                    }
                    <div>
                        <img onClick={e => { this.changeCode(e); }} className="code" src={"/api/imgCode?ver=" + codeVer} alt="" />
                        <Input value={code} onFocus={this.changeCode} onChange={e => { this.onChanges(e, 'code'); }} style={{ width: 176 }} size="lg" placeholder="验证码" />
                    </div>
                    {type === 'register' ?
                        <Button onClick={e => this.registerHand(e)} type="primary" size="lg" block={true}>注册</Button> :
                        <Button onClick={e => this.loginHand(e)} type="primary" size="lg" block={true}>登录</Button>
                    }
                </div>
            </div >
        );
    }
}