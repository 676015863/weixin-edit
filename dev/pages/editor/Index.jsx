import './style.scss';
import React, { Component } from 'react';
import { DIST } from 'conf';
import { Slider, Tip, LoadingModal, Switch } from 'mtui';
import { SketchPicker, CirclePicker } from 'react-color';

function isIE() { //ie?
    if (!!window.ActiveXObject || "ActiveXObject" in window)
        return true;
    else
        return false;
}
export default class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shadow: 'none', // 阴影
            moreColor: false,
            border: {
                width: 0,
                style: 'none',
                color: '#000000'
            },
            colorShow: false,
            size: {
                top: 0,
                left: 0
            },
            imgSize: {
                top: 0,
                left: 0
            },
            imgShow: false,
            show: false
        };
        this.ue = null;
        this.dom = null;
        this.img = null;
        this.clipboard = null;
        this.colors = ["#f44336", "#e91e63", "#9c27b0", "#673ab7", "#3f51b5", "#2196f3", "#03a9f4", "#00bcd4", "#009688"];
    }

    setUeHeight = num => {
        let winHei = $(window).height() - $('.edui-editor-toolbarboxouter').height() - num;
        this.ue.setHeight(winHei);
    }

    removeActiveClass() {
        this.dom && this.dom.closest('html').find('.active').removeClass('active');
    }

    componentWillMount() {
        if (!isIE()) {
            LoadingModal.show('loading');
        }
    }

    componentDidMount() {
        this.ue = UE.getEditor('container', {
            autoHeight: true
        });
        // console.log(this.ue);

        this.ue.on('ready', e => {
            this.setUeHeight(24);
            window.ue = this.ue;
            LoadingModal.hide();
        });

        $(window).on('resize.ueditor', () => {
            this.setUeHeight(26);
        });

        // 设置 dom
        $(document).on('clickRank', (e, dom) => {
            e.stopPropagation();
            this.dom = $(dom);
            this.removeActiveClass();
            this.dom.addClass('active');
            let stop = this.dom.closest('html').scrollTop();
            let top = this.dom.offset().top + this.dom.height() + 100 - stop;

            if (top + 120 > $(window).height()) {
                top -= (this.dom.height() + 120);
            }

            this.setState({
                show: true,
                imgShow: false,
                size: {
                    height: 160,
                    top: top,
                    left: this.dom.position().left
                }
            });

            // 滚动的时候
            this.dom.closest('html').off('mousewheel').on('mousewheel', e => {
                $(document).trigger('click.blank');
            });

        });

        // 设置 img
        $(document).on('clickImg', (e, dom) => {
            if ($(dom).closest('.rankImg')[0]) {
                this.img = $(dom).closest('.rankImg');
            } else {
                $(dom).wrap(`<span class="rankImg" style="display: inline-block; overflow: hidden; position: relative;"></span>`);
                this.img = $(dom).closest('.rankImg');
            }

            // 滚动的时候
            this.img.closest('html').off('mousewheel').on('mousewheel', e => {
                $(document).trigger('click.blank');
            });

            // 设置border， shadow
            this.setState({
                border: {
                    width: this.img.css('border-width'),
                    style: this.img.css('border-style'),
                    color: this.img.css('border-color')
                },
                shadow: this.img.css('box-shadow')
            });

            let stop = this.img.closest('html').scrollTop();
            let top = this.img.offset().top + this.img.height() + 100 - stop;

            if (top + 120 > $(window).height()) {
                top -= (this.img.height() + 120);
            }

            this.setState({
                show: false,
                imgShow: true,
                imgSize: {
                    stop: stop,
                    width: this.img.width(),
                    height: this.img.height(),
                    offset: this.img.offset(),
                    top: top,
                    left: this.img.position().left
                }
            });

        });

        // 点击空白处
        $(document).on('click.blank', e => {
            if (!$(e.target).closest('.setbox')[0]) {
                this.setState({
                    show: false,
                    imgShow: false
                });
                this.removeActiveClass();
            }
        });

    }

    // 设置变化
    silderChange = (data, type) => {
        // console.log(data, type);
        if (type === 'size') {
            this.dom.find('section').eq(0).css('width', data + '%')
        }
        if (type === 'opacity') {
            this.dom.css('opacity', data)
        }
        if (type === 'rotate') {
            this.dom.css('transform', `rotate(${data}deg)`)
        }
    }

    // 图片 设置变化
    silderImgChange = (data, type) => {
        if (type === 'radius') {
            this.img.css('border-radius', data + '%')
        }
        if (type === 'size') {
            this.img.css('width', data + '%')
        }
        if (type === 'opacity') {
            this.img.css('opacity', data)
        }
        if (type === 'rotate') {
            this.img.css('transform', `rotate(${data}deg)`)
        }
    }

    fastEvent = (type) => {
        if (type === 'prev') {
            this.dom.before('<p><br /></p>');
        }
        if (type === 'next') {
            this.dom.after('<p><br /></p>');
        }
        if (type === 'clear') {
            this.dom.find('[style]').each(function () {
                $(this).removeAttr('style');
            });
        }
        if (type === 'copy') {
            // 复制
            Tip.success('复制成功！');
            this.clipboard = this.dom ? this.dom.prop('outerHTML') : '';
        }
        if (type === 'paste') {
            this.dom.after(this.clipboard);
        }
        if (type === 'del') {
            this.dom.remove();
            $(document).trigger('click.blank');
        }

        // 上移，下移
        if (type === 'up') {
            let prev = this.dom.prev().prop('outerHTML');
            this.dom.prev().remove();
            this.dom.after(prev);
        }
        if (type === 'down') {
            let prev = this.dom.next().prop('outerHTML');
            this.dom.next().remove();
            this.dom.before(prev);
        }

        // 段落
        if (type === 'duanluo-prev') {
            this.dom.before('<section class="RankEditor"><p><br /></p></section>');
        }
        if (type === 'duanluo-next') {
            this.dom.after('<section class="RankEditor"><p><br /></p></section>');
        }
    }

    // 选择边框
    selectBorder = e => {
        this.img.css('border-style', e.target.value);
        this.state.border.style = e.target.value;
        this.setState({
            border: this.state.border
        });
    }

    // 边框宽度
    silderBorderChange = e => {
        console.log(e);
        this.img.css('border-width', e + 'px');
        this.state.border.width = e;
        this.setState({
            border: this.state.border
        });
    }

    // 边框颜色
    changeColor = color => {
        this.state.border.color = color.hex;
        this.setState({
            border: this.state.border
        });
        this.img.css('border-color', color.hex);
    }

    // 阴影
    selectShadow = e => {
        this.img.css('box-shadow', e.target.value);
        this.setState({
            shadow: e.target.value
        });
    }

    // 删除img
    removeImg = e => {
        this.img.remove();
        this.setState({
            imgShow: false
        });
        this.removeActiveClass();
    }

    componentWillUnmount() {
        this.dom = null;
        this.ue.off('ready');
        this.ue.destroy();
        $(document).off('click.blank');
        $(document).off('clickImg');
        $(document).off('clickRank');
        $(window).off('resize.ueditor');
        this.clipboard = null;
    }

    render() {
        let { size, show, imgShow, imgSize, colorShow, shadow, border, moreColor } = this.state;
        return (
            <div className="editor">
                <script id="container" name="content" type="text/plain">这里写你的初始化内容</script>
                {show ? (
                    <div className="setbox" style={{ ...size }}>
                        <div className="fastbtn">
                            <a onClick={e => this.fastEvent('prev')}>前空</a>
                            <a onClick={e => this.fastEvent('next')}>后空</a>
                            <a onClick={e => this.fastEvent('up')}>上移</a>
                            <a onClick={e => this.fastEvent('down')}>下移</a>
                            <a onClick={e => this.fastEvent('duanluo-prev')}>前段落</a>
                            <a onClick={e => this.fastEvent('duanluo-next')}>后段落</a>
                            <a className="copy-section" onClick={e => this.fastEvent('copy')}>复制</a>
                            <a onClick={e => this.fastEvent('paste')}>粘贴</a>
                            <a onClick={e => this.fastEvent('clear')}>清除格式</a>
                            <a onClick={e => this.fastEvent('del')}>删除</a>
                        </div>
                        <ul>
                            <li>宽度比：<Slider onChange={e => this.silderChange(e, 'size')} width={200} defaultValue={100} minValue={0} maxValue={100} /></li>
                            <li>旋转角：<Slider onChange={e => this.silderChange(e, 'rotate')} width={200} defaultValue={0} minValue={0} maxValue={360} /></li>
                            <li>透明度：<Slider onChange={e => this.silderChange(e, 'opacity')} width={200} defaultValue={1} minValue={0} maxValue={1} /></li>
                        </ul>
                    </div>
                ) : null}
                {imgShow ? (
                    <div className="imgoffsetbox" style={{
                        width: imgSize.width,
                        height: imgSize.height,
                        top: imgSize.offset.top - imgSize.stop + 80,
                        left: imgSize.offset.left
                    }}></div>
                ): null}
                {imgShow ? (
                    <div className="setbox" style={{
                        top: imgSize.top,
                        left: imgSize.left
                    }}>
                        <ul>
                            <li className="setitem">
                                <label>边框：</label>
                                <select value={border.style} onChange={this.selectBorder}>
                                    <option value="none">无</option>
                                    <option value="solid">实线</option>
                                    <option value="dashed">虚线</option>
                                    <option value="dotted">点</option>
                                    <option value="double">双线</option>
                                </select>
                                <Slider onChange={this.silderBorderChange} width={70} defaultValue={parseInt(border.width, 10)} minValue={0} maxValue={50} />
                                <br />
                                <br />
                                <span className="span"><CirclePicker color={border.color} onChange={this.changeColor} circleSize={16} circleSpacing={5} width={200} colors={this.colors} /></span>
                                <span className="more"><i onClick={e => { this.setState({ moreColor: !moreColor }) }}>more</i>
                                    <div className="colors" style={{ display: moreColor ? 'block' : 'none' }}>
                                        <SketchPicker color={border.color} onChange={this.changeColor} />
                                    </div>
                                </span>
                            </li>
                            <li className="setitem">
                                <label>阴影：</label>
                                <select style={{ width: 120 }} value={shadow} onChange={this.selectShadow}>
                                    <option value="none">无阴影</option>
                                    <option value="rgba(0, 0, 0, 0.6) 0px -20px 20px 0px">上</option>
                                    <option value="rgba(0, 0, 0, 0.6) 0px 20px 20px 0px">下</option>
                                    <option value="rgba(0, 0, 0, 0.6) -20px 0px 20px 0px">左</option>
                                    <option value="rgba(0, 0, 0, 0.6) 20px 0px 20px 0px">右</option>
                                </select>
                            </li>
                            <li className="setitem"></li>
                            <li><label>圆角：</label><Slider onChange={e => this.silderImgChange(e, 'radius')} width={180} defaultValue={0} minValue={0} maxValue={100} /></li>
                            <li><label>大小：</label><Slider onChange={e => this.silderImgChange(e, 'size')} width={180} defaultValue={100} minValue={0} maxValue={100} /></li>
                            <li><label>旋转角：</label><Slider onChange={e => this.silderImgChange(e, 'rotate')} width={180} defaultValue={0} minValue={0} maxValue={360} /></li>
                            <li><label>透明度：</label><Slider onChange={e => this.silderImgChange(e, 'opacity')} width={180} defaultValue={1} minValue={0} maxValue={1} /></li>
                            <li><a onClick={this.removeImg}>删除</a></li>
                        </ul>
                    </div>
                ) : null}
            </div>
        );
    }
}