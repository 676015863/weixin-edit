import './style.scss';
import React, { Component } from 'react';
import { Link } from 'react-router';
import Upload from 'rc-upload';
import Frame from '../common/Frame';
import { Panel, Input, Button, Modal, Tip } from 'mtui';
import Pages from '../common/pages';
import Tags from '../common/tags';
import { getImgs, getTypes, delImgs, saveImgs } from '@/server/source';
import Search from '../common/Search';

export default class SourceImgs extends Component {
    // 构造函数
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            infos: {},
            types: null
        };
        this.modalRef = null;
        this.selectType = '';
        this.current = 0;
        this.elem = null;
    }

    // 模拟 ajax 异步
    ajaxDo = (current, refresh, name) => {
        this.current = current;
        return getImgs({
            pageSize: 20,
            pageNum: current,
            name: name || '',
            type: this.selectType
        }).then(res => {
            // console.log('这里写东西~', this);
            this.setState({
                data: res.data || []
            });
            return res;
        });
    }

    // tag 选择
    changeTag = (e) => {
        // console.log(e);
        this.selectType = e.id;
        this.ajaxDo(1);
    }

    // 删除
    delData = (elem) => {
        this.modalRef.showModal(true);
        this.elem = elem;
    }

    // 确定删除
    sureDel = () => {
        delImgs({ id: this.elem.id }).then(res => {
            if (res.success) {
                Tip.success('操作成功！');
                this.ajaxDo(this.current);
            } else {
                Tip.error('操作失败！');
            }
            this.modalRef.showModal(false);
        });
    }

    // 获取分类
    getTypes = () => {
        getTypes({
            type: 0
        }).then(res => {
            // ...
            this.setState({
                types: res.data
            });
        });
    }

    uploadSuccess = (res) => {
        console.log(res);
        let d = res.data;
        saveImgs({
            name: d.name,
            url: d.url,
            size: parseInt(d.size / 1000, 10) + 'kb',
            type: this.selectType
        }).then( resp => {
            if (resp.success) {
                Tip.success('操作成功！');
                this.ajaxDo(1);
            } else {
                Tip.error('操作失败！');
            }
        });
    }

    beforeUpload = (e) => {
        // ...
    }

    componentDidMount() {
        this.getTypes();
    }

    render() {
        let { types, data } = this.state;
        return (
            <div className="contents apptpl apptpl-h5">
                <div className="contents-head">
                    <div className="search-box">
                        <Search ajaxDo={this.ajaxDo}/>
                        <Upload action="/api/upload" beforeUpload={ e => {this.beforeUpload(e);}} onSuccess={e => { this.uploadSuccess(e); }} multiple={true}><Button prefix={<i className="fonts icon-shangchuan1"></i>} type="warning"> 图片上传</Button></Upload>
                    </div>
                    {types ? <Tags types={types} change={this.changeTag} className="apptpl-case-type" /> : null}
                </div>
                <div className="contents-body">
                    <div className="table" style={{ display: data ? 'block' : 'none' }}>
                        <table className="mt-table">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>缩图</th>
                                    <th>名称</th>
                                    <th>分类</th>
                                    <th>上传时间</th>
                                    <th>大小</th>
                                    <th style={{ width: 100 }}>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    data && data.map((elem, index) => {
                                        return <tr key={index}>
                                            <td>{elem.id}</td>
                                            <td><a href={elem.url} target="_blank"><img title={elem.url} src={elem.url} style={{ width: 60, height: 40 }} alt="" /></a></td>
                                            <td>{elem.name}</td>
                                            <td>{elem.type}</td>
                                            <td>{elem.date}</td>
                                            <td>{elem.size}</td>
                                            <td>
                                                <Button onClick={e => { this.delData(elem); }} size="sm" type="danger">删除图片</Button> &nbsp;
                                            </td>
                                        </tr>;
                                    })
                                }
                            </tbody>
                        </table>
                        <Pages ajax={this.ajaxDo} />
                    </div>
                </div>
                <Modal ref={e => { this.modalRef = e; }} style={{ width: 240, height: 140 }}>
                    <div className="mt-panel-min modal-sure">
                        <div className="mt-panel-box">
                            <p>是否删除图片？删除后不可恢复</p>
                            <div className="modal-btns">
                                <Button onClick={e => { this.modalRef.showModal(false); }}>取消</Button>
                                <Button onClick={e => { this.sureDel(e); }} type="success">确定</Button>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}