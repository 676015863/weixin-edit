import './style.scss';
import React, { Component } from 'react';
import { Link } from 'react-router';
import Upload from 'rc-upload';
import Frame from '../Common/Frame';
import { Panel, Input, Button, Modal, Tip, Limit, Radio } from 'mtui';
import Pages from '../Common/pages';
import Tags from '../Common/tags';
import { getTpls, getTypes, delTpl, saveTpl, updateTpl } from '@/server/source';
import Search from '../Common/Search';

const RadioGroup = Radio.RadioGroup;

export default class SourceHtmls extends Component {
    // 构造函数
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            infos: {},
            types: null,
            editElem: null // 编辑用
        };
        this.modalRef = null;
        this.modalBoxRef = null;
        this.selectType = '';
        this.current = 0;
        this.elem = null; // 删除用
    }

    // 模拟 ajax 异步
    ajaxDo = (current, refresh, name) => {
        this.current = current;
        return getTpls({
            pageSize: 20,
            pageNum: current,
            name: name || '',
            type: this.selectType
        }).then(res => {
            // console.log('这里写东西~', this);
            this.setState({
                data: res.data || []
            });
            return res;
        });
    }

    // tag 选择
    changeTag = (e) => {
        // console.log(e);
        this.selectType = e.id;
        this.ajaxDo(1);
    }

    // 编辑
    editData = (elem) => {
        this.modalBoxRef.showModal(true);
        this.setState({ editElem: elem });
    }

    // 删除
    delData = (elem) => {
        this.modalRef.showModal(true);
        this.elem = elem;
    }

    // 确定删除
    sureDel = () => {
        delTpl({ id: this.elem.id }).then(res => {
            if (res.success) {
                Tip.success('操作成功！');
                this.ajaxDo(this.current);
            } else {
                Tip.error('操作失败！');
            }
            this.modalRef.showModal(false);
        });
    }

    // 获取分类
    getTypes = () => {
        getTypes({ type: 1 }).then(res => {
            let arr = res.data;
            arr.push({ id: 12, type: 2, name: "整套模板" });
            this.setState({
                types: arr.reverse()
            });
        });
    }

    uploadSuccess = (res) => {
        // console.log('>>>>>>>>>', res);
        let { editElem } = this.state;
        editElem.pic = res.data.url;
        this.setState({ editElem });
    }

    // 修改数据
    changeVal = (e, name) => {
        let obj = {};
        obj[name] = e.value ? e.value : e.target.value;
        obj = Object.assign(this.state.editElem, obj);
        this.setState({ editElem: obj });
    }

    // 确认修改
    sureUpdate = (e) => {
        // console.log(this.state.editElem);
        let { editElem } = this.state;
        if (editElem.id === '') {
            // 添加新的
            saveTpl(editElem).then(res => {
                if (res.success) {
                    Tip.success('新增成功！');
                } else {
                    Tip.error('添加失败');
                }
                this.modalBoxRef.showModal(false);
            });
        } else {
            updateTpl(editElem).then(res => {
                if (res.success) {
                    Tip.success('修改成功！');
                } else {
                    Tip.error('修改失败');
                }
                this.modalBoxRef.showModal(false);
            });
        }
        this.ajaxDo(1);
    }

    componentDidMount() {
        this.getTypes();
    }

    render() {
        let { types, data, editElem } = this.state;
        return (
            <div className="contents apptpl apptpl-h5">
                <div className="contents-head">
                    <div className="search-box">
                        <Search ajaxDo={this.ajaxDo} />
                        <Button onClick={e => { this.editData({ id: '', name: '', pic: '', des: '', data: '', type: '' }); }} prefix={<i className="fonts icon-add"></i>} type="warning"> 新建模块</Button>
                    </div>
                    {types ? <Tags types={types} change={this.changeTag} className="apptpl-case-type" /> : null}
                </div>
                <div className="contents-body">
                    <div className="table" style={{ display: data ? 'block' : 'none' }}>
                        <table className="mt-table">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>缩图</th>
                                    <th>名称</th>
                                    <th>分类</th>
                                    <th>上传时间</th>
                                    <th>数据</th>
                                    <th>描述</th>
                                    <th style={{ width: 200 }}>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    data && data.map((elem, index) => {
                                        return <tr key={index}>
                                            <td>{elem.id}</td>
                                            <td><a href={elem.pic} target="_blank"><img title={elem.pic} src={elem.pic} style={{ width: 60, height: 40 }} alt="" /></a></td>
                                            <td>{elem.name}</td>
                                            <td>{elem.type}</td>
                                            <td>{elem.date}</td>
                                            <td><Limit onClick={e => { console.log(elem.data); }} size={20}>{elem.data}</Limit></td>
                                            <td><Limit size="10">{elem.des || '-'}</Limit></td>
                                            <td>
                                                <Button onClick={e => { this.editData(elem); }} size="sm" type="success">编辑模板</Button> &nbsp;
                                                <Button onClick={e => { this.delData(elem); }} size="sm" type="danger">删除模板</Button> &nbsp;
                                            </td>
                                        </tr>;
                                    })
                                }
                            </tbody>
                        </table>
                        <Pages ajax={this.ajaxDo} />
                    </div>
                </div>
                <Modal ref={e => { this.modalRef = e; }} style={{ width: 240, height: 140 }}>
                    <div className="mt-panel-min modal-sure">
                        <div className="mt-panel-box">
                            <p>是否删除图片？删除后不可恢复</p>
                            <div className="modal-btns">
                                <Button onClick={e => { this.modalRef.showModal(false); }}>取消</Button>
                                <Button onClick={e => { this.sureDel(e); }} type="success">确定</Button>
                            </div>
                        </div>
                    </div>
                </Modal>

                <Modal ref={e => { this.modalBoxRef = e; }} style={{ width: 340, height: 440 }}>
                    <div className="mt-panel-min modal-sure">
                        <Panel size="min" header="编辑模板">
                            <div className="modal-body">
                                {editElem ?
                                    <div className="source-tpl-edit">
                                        <ul>
                                            <li><label>名字：</label><Input
                                                onChange={e => { this.changeVal(e, 'name'); }}
                                                value={editElem.name} /></li>
                                            <li><label>缩图：</label><Input disabled
                                                value={editElem.pic} />
                                                &nbsp;<Upload action="/api/upload" onSuccess={e => { this.uploadSuccess(e); }}><Button type="warning" size="sm">上传</Button></Upload>
                                            </li>
                                            <li><label>分类：</label>
                                                <RadioGroup
                                                    onChange={e => { this.changeVal(e, 'type'); }}
                                                    type="button"
                                                    value={editElem.type || 0} >
                                                    {
                                                        types && types.map((elem, index) => {
                                                            return <Radio key={index} value={elem.id}>{elem.name}</Radio>;
                                                        })
                                                    }
                                                </RadioGroup>
                                            </li>
                                            <li><label>描述：</label><Input onChange={e => { this.changeVal(e, 'des'); }} className="des" type="textarea" value={editElem.des} /></li>
                                            <li><label>数据：</label><Input onChange={e => { this.changeVal(e, 'data'); }} type="textarea" value={editElem.data} /></li>
                                        </ul>
                                    </div> : null}
                            </div>
                            <div className="modal-btns">
                                <Button onClick={e => { this.modalBoxRef.showModal(false); }}>取消</Button>
                                <Button onClick={e => { this.sureUpdate(e); }} type="success">确定</Button>
                            </div>
                        </Panel>
                    </div>
                </Modal>
            </div >
        );
    }
}