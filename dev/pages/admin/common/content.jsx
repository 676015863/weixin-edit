import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

export default class Content extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="content">
            <div className="content-title">
                {this.props.title}
            </div>
            <div className="content-body">
                {this.props.children}
            </div>
        </div>;
    }
}