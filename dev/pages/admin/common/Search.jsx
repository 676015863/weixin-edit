import './style.scss';
import React, { Component } from 'react';
import { Input, Button, Tip } from 'mtui';

export default class Search extends Component {
    // 构造函数
    constructor(props) {
        super(props);
        this.state = {
            name: props.name || ''
        };
    }

    onChange = (e) => {
        this.setState({
            name: e.target.value
        });
    }

    render() {
        return (
            <Input className={this.props.className ? this.props.className : ''} onChange={this.onChange} value={this.state.name} size="lg" placeholder="名称搜索" suffix={
                    <a onClick={e => { this.props.ajaxDo(1, null, this.state.name); }}><i className="iconfont icon-search"></i></a>} />
        );
    }
}