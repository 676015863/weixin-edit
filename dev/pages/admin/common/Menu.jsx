import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { Tabs } from 'mtui/index';
import { DIST } from 'conf';

const TabItem = Tabs.TabItem;

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0
        };
    }

    componentDidMount() {

        let index = $('[href="' + location.pathname + '"]').closest('.mt-tabs-item').index();

        this.setState({
            index: index
        });
    }

    render() {
        return <div className="menu">
            <div className="navlist">
                <ul className="links">
                    <li><Link activeClassName="active" to={DIST + "/admin/user"}>用户管理</Link></li>
                    <li><Link activeClassName="active" to={DIST + "/admin/imgs"}>图片素材</Link></li>
                    <li><Link activeClassName="active" to={DIST + "/admin/tpls"}>模板素材</Link></li>
                    {/* <li><Link activeClassName="active" to={DIST + "/admin/article"}>文章素材</Link></li> */}
                    <li><Link activeClassName="active" to={DIST + "/admin/repassword"}>修改密码</Link></li>
                    <li><a href="/api/logout">退出系统</a></li>
                </ul>
            </div>
        </div>;
    }
}
export default Menu;