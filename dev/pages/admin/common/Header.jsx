import './style.scss';
import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
// import logo from '../../assets/imgs/logo.png';
import { DIST } from 'conf';

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="header">
            <div className="headerbox">
                <div className="logo">
                    <Link activeClassName="active" to={DIST}>后台系统</Link>
                </div>
                <div className="menus">
                    <div className="btns">
                        <a>欢迎登录！</a>
                        <Link to={DIST + '/'}>编辑器</Link>
                        <a href='/api/logout'>退出系统</a>
                    </div>
                </div>
            </div>
        </div>;
    }
}
export default Header;