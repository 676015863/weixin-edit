import './style.scss';
import React, { Component } from 'react';
import { Input, Button } from 'mtui';

export default class Tags extends Component {
    // 构造函数
    constructor(props) {
        super(props);
        this.state = {
            del: false,
            all: true,
            types: props.types || []
        };
    }

    // 编辑标签
    editTag() {
        // console.log('del');
        let { del, types } = this.state;
        if (!del) {
            console.log('编辑');
        } else {

            if(this.props.edit) {
                this.props.edit(types);
            }
        }
        this.setState({
            del: !del
        });
    }

    // 选择标签
    selectTag(e, elem) {
        let { change } = this.props;
        let { del, types } = this.state;
        for(let d of types) {
            d.active = false;
        }
        // 选择全部
        if(elem === 'all' && !del) {
            // console.log('all');
            if(change) {
                change('all');
            }
            this.setState({
                all: true
            });
            return;
        }

        if (!del) {
            elem.active = true;
            if(change) {
                change(elem);
            }
            this.setState({
                all: false,
                types: types
            });
        }
    }

    // 修改标签名称
    changeTagName(e, index) {
        let { types } = this.state;
        types[index].name = e.target.value;
        this.setState({
            types: types
        });
    }

    // 删除标签
    delTag(e, index) {
        let { types } = this.state;
        types.splice(index, 1);
        this.setState({
            types: types
        });
    }

    // 添加标签
    addTag(e) {
        let { types } = this.state;
        types.push({ name: '标签名称', id: null });
        this.setState({
            types: types
        });
    }

    render() {
        let { del, types, all } = this.state;
        let { className } = this.props;
        let cName = ['tags-box'];
        if(className) {
            cName.push(className);
        }
        return (
            <div className={cName.join(' ')}>
                <Button type={ all ? 'success' : 'default'} dom="span" onClick={e => { this.selectTag(e, 'all'); }}>全部</Button>
                <span className="tags">
                    {
                        types.map((elem, index) => {
                            return (
                                <Button type={ elem.active ? 'success' : 'default'} onClick={e => { this.selectTag(e, elem); }} key={index} dom="span">
                                    {
                                        del ? <Input onChange={e => { this.changeTagName(e, index); }} size="xs" type="text" value={elem.name} /> : <span>{elem.name}</span>
                                    }
                                    {
                                        del ? <a onClick={e => { this.delTag(e, index); }} className="del"><i className="iconfont icon-close"></i></a> : null
                                    }
                                </Button>
                            );
                        })
                    }
                </span>
                {/* {del ? <Button onClick={e => { this.addTag(e); }}>+</Button> : null}
                <Button onClick={e => { this.editTag(e); }} type="primary">{del ? '保存标签' : '编辑标签'}</Button> */}
            </div>
        );
    }
}