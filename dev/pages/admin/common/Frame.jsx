import React, { Component } from 'react';
import Header from './Header';
import Menu from './Menu';

export default class Frame extends Component {
    render() {
        let styleArr = ['frame'];
        if(this.props.className) {
            styleArr.push(this.props.className);
        }
        return (
            <div className={styleArr.join(' ')}>
                <Header />
                <div className="frame-left">
                    <Menu />
                </div>
                <div className="frame-body">
                    <div className="frame-content clearfix">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}