import './style.scss';
import React, { Component } from 'react';
import { Link } from 'react-router';
import Frame from '../common/Frame';
import { Panel, Input, Button, Tip, Modal } from 'mtui';
import Pages from '../common/pages';
import { getUserList, resetPassword, delUser } from '@/server/user';

export default class UserNormal extends Component {
    // 构造函数
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            keys: ''
        };
        this.modalRef = null;
        this.elem = null;
    }

    // 获取用户列表
    ajaxDo = (current, refresh) => {
        return getUserList({
            tel: this.state.keys || '',
            pageSize: 20,
            pageNum: current
        }).then(res => {
            this.setState({
                data: res.data || []
            });
            return res;
        });
    }

    // 重置密码
    resetPassword(elem) {
        let num = '123456';
        resetPassword({
            id: elem.id,
            password: num
        }).then(res => {
            console.log(res);
            if (res.success) {
                Tip.success('重置密码成功！密码为' + num);
            }
        });
    }

    // 禁用, 启动账号
    delUser(elem, mark) {
        this.elem = elem;
        this.modalRef.showModal(true);
    }

    sureDel(elem) {
        delUser({
            id: elem.id
        }).then(res => {
            // ...
            // elem.del = (mark ? 1 : 0);
            this.setState(this.state.data);
        });
    }

    // 搜索
    searchDo(e) {
        this.ajaxDo(1);
    }

    onChangeDo(e) {
        this.setState({
            keys: e.target.value
        });
    }

    render() {
        return (
            <div className="contents user user-normal">
                <div className="contents-head">
                    <Input value={this.state.keys} onChange={e => { this.onChangeDo(e); }} size="lg" placeholder="手机号搜索" suffix={<a onClick={e => { this.searchDo(e); }}><i className="iconfont icon-search"></i></a>} />
                </div>
                <div className="contents-body">
                    <div className="table">
                        <table className="mt-table">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    {/* <th>用户名</th> */}
                                    {/* <th>email</th> */}
                                    <th>用户（电话）</th>
                                    <th>注册时间</th>
                                    <th style={{ width: 260 }}>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.data.map((elem, index) => {
                                        return <tr key={index}>
                                            <td>{elem.id}</td>
                                            {/* <td>{elem.username}</td> */}
                                            {/* <td>{elem.email}</td> */}
                                            <td>{elem.tel}</td>
                                            <td>{elem.updateTime}</td>
                                            <td>
                                                <Button onClick={e => { this.resetPassword(elem); }} size="sm" type="info">重置密码</Button> &nbsp;
                                                {!elem.del ?
                                                    <Button onClick={e => { this.delUser(elem, true); }} size="sm" type="danger">删除账号</Button> : <Button onClick={e => { this.delUser(elem, false); }} size="sm" type="warning">启用账号</Button>}
                                            </td>
                                        </tr>;
                                    })
                                }
                            </tbody>
                        </table>
                        <Pages ajax={this.ajaxDo} />
                    </div>
                </div>

                <Modal ref={e => { this.modalRef = e; }} style={{ width: 240, height: 140 }}>
                    <div className="mt-panel-min modal-sure">
                        <div className="mt-panel-box">
                            <p>是否删除用户？删除后不可恢复</p>
                            <div className="modal-btns">
                                <Button onClick={e => { this.modalRef.showModal(false); }}>取消</Button>
                                <Button onClick={e => { this.sureDel(this.elem); }} type="success">确定</Button>
                            </div>
                        </div>
                    </div>
                </Modal>

            </div>
        );
    }
}