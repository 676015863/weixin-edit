import './style.scss';
import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import NotFound from './common/NotFound';
import { getLocal } from '@/utils/storage';
import { DIST } from 'conf';
import { getUser } from '@/server/user';

export default class Index extends Component {
    // 构造函数
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        // xx
        getUser();
        let user = getLocal('user') || {};
        if (user.usertype === 0) {
            // console.log(this.user);
        } else {
            browserHistory.push(DIST + '/');
        }
    }

    render() {
        return (
            <div className="admin">
                {this.props.children}
            </div>
        );;
    }
}