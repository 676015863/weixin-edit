import './style.scss';
import React, { Component } from 'react';
import { Link } from 'react-router';
import { Input, Button } from 'mtui';

export default class ManagerRepassword extends Component {
    // 构造函数
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            repassword: ''
        }
    }

    changePas = () => {
        let { password, repassword } = this.state;
        if (password !== repassword) {
            Tip.error('两次密码不一样！');
            return;
        }
        resetPassword({
            password: password
        }).then(res => {
            // console.log(res);
            if (res.success) {
                Tip.success('重置密码成功！');
            }
        });
    }

    changeVal = (e, key) => {
        let obj = {};
        obj[key] = e.target.value;
        this.setState(obj);
    }

    render() {
        let { password, repassword } = this.state;
        return (
            <div className="contents manager manager-repassword">
                <Input placeholder="新密码" size="lg" onChange={e => this.changeVal(e, 'password')} type="text" value={password} /><br/><br/>
                <Input placeholder="重复密码" size="lg" onChange={e => this.changeVal(e, 'repassword')} type="text" value={repassword} /><br/><br/>
                <Button width={167} type="success" onClick={this.changePas}>确认修改</Button>
            </div >
        );
    }
}