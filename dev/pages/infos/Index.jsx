import './style.scss';
import React, { Component } from 'react';
import { DIST } from 'conf';
import { Input, Button, Modal, Tip } from 'mtui';
import Upload from 'rc-upload';
import { getLocal } from '@/utils/storage';
import { saveArticle, updateArticle } from '@/server/article';
import Clipboard from 'clipboard';

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pic: null,
            author: '',
            name: '',
            des: '',
            data: ''
        }
        this.modalRef = null;
    }

    saveArticle = () => {
        let user = getLocal('user');
        if (user) {

            let { pic, author, name, des } = this.state;

            if (!pic || !name) {
                Tip.error('主图和标题必须填写！');
                return;
            }

            // 保存文章
            this.state.data = UE.getEditor('container').getAllHtml() + '';

            let id = $('#container').attr('data-id');
            if(id !== undefined) {
                updateArticle({id, ...this.state}).then(res => {
                    if (res.success) {
                        Tip.success('保存成功！');
                        $(document).trigger('saveSucess');
                    } else if (res.code === 403) {
                        $(document).trigger('showLogin');
                    }
                });
            } else {
                saveArticle(this.state).then(res => {
                    if (res.success) {
                        Tip.success('保存成功！');
                        $(document).trigger('saveSucess');
                    } else if (res.code === 403) {
                        $(document).trigger('showLogin');
                    }
                });
            }
        } else {
            $(document).trigger('showLogin');
        }
    }

    // 上传成功
    uploadSuccess = res => {
        this.setState({
            pic: res.data.url
        });
    }

    // 数据change
    changeVal = (e, key) => {
        let obj = {};
        obj[key] = e.target.value;
        this.setState(obj);
    }

    // 清空编辑器
    clearEditor = () => {
        this.setState({
            pic: null,
            author: '',
            name: '',
            des: '',
            data: ''
        });
        $('#container').removeAttr('data-id');
        UE.getEditor('container').setContent('', false);
    }

    // 预览
    viewEditor = () => {
        this.modalRef.showModal(true);
        let style = `<style>.RankEditor{ border: none!important;}</style>`;
        let str = UE.getEditor('container').getAllHtml();
        setTimeout(() => {
            $('#windowframe').contents().find("body").html(style + str);
        }, 100);
    }

    componentDidMount() {
        // 复制
        this.clipboard = new Clipboard('.copys', {
            text: function (trigger) {
                Tip.success('复制成功！');
                return UE.getEditor('container').getAllHtml();
            }
        });

        $(document).on('editArticle', (e, data) => {
            this.setState({
                pic: data.pic,
                author: data.author,
                name: data.name,
                des: data.des
            });
        });
    }

    componentWillUnmount() {
        this.clipboard.destroy();
        $(document).off('editArticle');
    }

    render() {
        let { pic, name, author, des } = this.state;
        return (
            <div className="infos">
                <Input size="lg" placeholder="标题" block={true} value={name} onChange={e => { this.changeVal(e, 'name'); }} />
                <Input size="lg" placeholder="作者" block={true} value={author} onChange={e => { this.changeVal(e, 'author'); }} />
                <Upload action="/api/upload" onSuccess={this.uploadSuccess}>
                    <div className="upload-img">
                        {
                            pic ? <img src={pic} alt="" /> : (
                                <span>
                                    <i className="wxfont ico-wx-shangchuan"></i>
                                    <p>封面上传</p>
                                </span>
                            )
                        }
                    </div>
                </Upload>
                <Input size="lg" placeholder="摘要信息" type="textarea" block={true} value={des} onChange={e => { this.changeVal(e, 'des'); }} />
                <Button onClick={this.saveArticle} type="success" block={true}>保存文章</Button>
                <ul className="tags-btn">
                    <li onClick={this.viewEditor}>预览</li>
                    <li className="copys">复制</li>
                    <li onClick={this.clearEditor}>清空</li>
                    <li onClick={this.clearEditor}>新建</li>
                </ul>

                <Modal ref={c => { this.modalRef = c; }} style={{width: 360, height: 658}} className="phoneview">
                    <div className="phone">
                        <div className="window">
                            <iframe width="100%" height="100%" frameBorder="0" id="windowframe"></iframe>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}