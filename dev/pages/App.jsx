import 'mtui/style.css';
import './layout.scss';

import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { DIST } from 'conf';

import Editor from './editor/Index';
import Infos from './infos/Index';
import Login from './login/Index';
import Frame from './common/Frame';
import AdminFrame from './admin/common/Frame';
import { getUser } from '@/server/user';

class App extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        // 登录过滤
        // if(!getLocal('user')) {
        //     browserHistory.push('/');
        // }
        getUser();
    }

    render() {
        let path = this.props.location.pathname.toLowerCase().replace(/\s/g, '');
        
        if (path.indexOf('admin') !== -1) {
            return (
                <AdminFrame>
                    {this.props.children}
                </AdminFrame>
            );
        }

        return (
            <Frame>
                <div className="left">
                    <ul className="tabs">
                        <li><Link activeClassName="active" to={DIST + '/styletpl'}>样式模板</Link></li>
                        <li><Link activeClassName="active" to={DIST + '/completetpl'}>整套模板</Link></li>
                        <li><Link activeClassName="active" to={DIST + '/imgsource'}>图片素材</Link></li>
                        <li><Link activeClassName="active" to={DIST + '/myarticle'}>我的文章</Link></li>
                    </ul>
                    {this.props.children}
                </div>
                <div className="right">
                    <div className="center">
                        <Editor />
                        <Infos />
                    </div>
                </div>
                <Login />
            </Frame>
        );
    }
}

// APP入口
export default App;