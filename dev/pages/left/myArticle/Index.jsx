import './style.scss';
import React, { Component } from 'react';
import { DIST } from 'conf';
import Listbox from '../../common/Listbox';
import Pages from '../../common/Pages';
import { getLocal } from '@/utils/storage';
import { getArticle, delArticle } from '@/server/article';
import { Modal, Panel, Button, Tip } from 'mtui';

export default class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: getLocal('user') ? true : false,
            data: [],
            actionType: null,
            elem: null,
            newPage: +new Date()
        }
        this.modal1 = null;
    }

    // 分页
    pageDo = (num) => {
        return getArticle({
            pageSize: 20,
            pageNum: num
        }).then(res => {
            if (res.success) {
                this.setState({
                    data: res.data
                });
                return res;
            }else if(res.code === 403) {
                this.setState({
                    login: false
                });
            }
        });
    }

    // 编辑文章
    editArticle = elem => {
        this.modal1.showModal(true);
        this.setState({
            actionType: 'edit',
            elem: elem
        });
    }

    // 删除文章
    delArticle = elem => {
        this.modal1.showModal(true);
        this.setState({
            actionType: 'del',
            elem: elem
        });
    }

    // 确认操作
    toSure = () => {
        let { actionType, elem } = this.state;
        if (actionType === 'edit') {
            $('#container').attr('data-id', elem.id);
            $(document).trigger('editArticle', elem);
            UE.getEditor('container').setContent(elem.data, false);
        } else {
            // 
            delArticle({
                id: elem.id
            }).then(res => {
                if (res.success) {
                    Tip.success('删除成功！');
                    this.pageDo(1);
                }
            });
            this.setState({
                newPage: +new Date()
            });
        }
        this.modal1.showModal(false);
    }

    // 弹窗关闭，释放内存
    closeBack = () => {
        this.setState({
            actionType: null,
            elem: null
        });
    }

    componentDidMount() {
        $(document).on('login', (e, data) => {
            this.setState({
                login: data
            });
        });

        $(document).on('saveSucess', data => {
            this.pageDo(1);
            this.setState({
                newPage: +new Date()
            });
        });
    }

    componentWillUnmount() {
        $(document).off('login');
        $(document).off('saveSucess');
    }

    render() {
        let { login, data, actionType, newPage } = this.state;
        return (
            <div className="myarticle">
                {
                    login ?
                        <div className="has-article">
                            <Listbox top={60}>
                                <ul className="clearfix article-list">
                                    {
                                        data.map(elem => {
                                            return (
                                                <li key={elem.id}>
                                                    <img src={elem.pic} alt="" />
                                                    <h1>{elem.name}</h1>
                                                    <p>{elem.des}</p>
                                                    <span>
                                                        更新时间 {elem.date}
                                                        <a onClick={e => { this.editArticle(elem); }} className="edit">编辑</a>
                                                        <a onClick={e => { this.delArticle(elem); }} className="del">删除</a>
                                                    </span>
                                                </li>
                                            )
                                        })
                                    }
                                    {
                                        data.length === 0 ? (
                                            <div className="no-article">
                                                <i className="wxfont ico-wx-empty"></i>
                                                <p className="null">空空如也~</p>
                                            </div>
                                        ) : null
                                    }
                                </ul>
                            </Listbox>
                            <Pages key={newPage} ajax={this.pageDo} />
                        </div> :
                        <div className="no-article">
                            <i className="wxfont ico-wx-meiyoubiaoqing"></i>
                            <p>您还没登录，请<a onClick={e => $(document).trigger('showLogin')}>登录</a></p>
                        </div>
                }
                <Modal ref={c => { this.modal1 = c; }} style={{ width: 300, height: 180 }} colseBack={this.closeBack}>
                    <Panel className="my-article-panel" header="确认信息">
                        <p>
                            {
                                actionType === 'del' ? '删除后无法恢复' : '编辑文章将替换现有的内容，是否清空当前的数据？'
                            }
                        </p>
                        <br />
                        <div className="btns">
                            <Button onClick={this.toSure} type="success">确认</Button> &nbsp;&nbsp;
                            <Button onClick={e => { this.modal1.showModal(false); }}>取消</Button>
                        </div>
                    </Panel>
                </Modal>
            </div >
        );
    }
}