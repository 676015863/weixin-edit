import './style.scss';
import React, { Component } from 'react';
import { DIST } from 'conf';
import Listbox from '../../common/Listbox';
import Pages from '../../common/Pages';
import { Button } from 'mtui';

import { getTpls } from '@/server/source';

export default class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            blockStr: null // 分块数据
        };
    }

    // 获取模板
    getTpls = (num) => {
        return getTpls({
            type: 12,
            pageSize: 20,
            pageNum: num || 1
        }).then(res => {
            if (res.success) {
                this.setState({
                    data: res.data
                });
                return res;
            }
        });
    }

    // 设置模板 
    selectTpl = elem => {
        let $dom = $('<div>' + elem.data + '</div>');
        $dom.find('.RankEditor').each(function() {
            $(this).removeClass('active');
            $(this).removeAttr('style');
        });
        UE.getEditor('container').setContent($dom.html(), true);
    }

    // 分块使用
    toBlock = elem => {
        let $dom = $('<div>' + elem.data + '</div>');
        $dom.find('.RankEditor').each(function() {
            $(this).removeClass('active');
            $(this).removeAttr('style');
        });
        this.setState({
            blockStr: $dom.html()
        });
    }

    componentDidMount() {
        $('#blockStr').on('click', '.RankEditor', function() {
            let str = $(this).prop('outerHTML');
            UE.getEditor('container').setContent(str, true);
        });
    }

    componentWillUnmount() {
        $('#blockStr').off('click');
    }

    render() {
        let { data, blockStr } = this.state;
        return (
            <div className="completetpl">
                <Listbox top={60}>
                    <ul className="tpls">
                        {
                            data.map(elem => {
                                return (
                                    <li key={elem.id}>
                                        <div className="btns">
                                            <Button onClick={e => { this.selectTpl(elem); }} type="success">使用整套</Button>
                                            <Button onClick={e => { this.toBlock(elem); }}>分块使用</Button>
                                        </div>
                                        <img src={elem.pic} />
                                    </li>
                                )
                            })
                        }
                    </ul>
                </Listbox>
                {
                    blockStr ? <a className="close-block" onClick={e => { this.setState({ blockStr: null }); }}><i className="iconfont icon-close"></i></a> : null
                }
                <div id="blockStr">
                {
                    blockStr ? (
                        <Listbox bottom={0} top={60} zIndex={10} className="block-str">
                            <div dangerouslySetInnerHTML={{ __html: blockStr }}></div>
                        </Listbox>
                    ) : null
                }
                </div>

                <Pages ajax={this.getTpls} />
            </div>
        );
    }
}