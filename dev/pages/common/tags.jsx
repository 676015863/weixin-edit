import React, { Component } from 'react';

export default class Tags extends Component {

    constructor(props) {
        super(props);
        this.state = {
            active: props.active
        }
    }

    // 选择tags
    clickTags = elem => {
        this.setState({
            active: elem.name
        });
        if(this.props.onChange) {
            this.props.onChange(elem);
        }
    }

    render() {
        let { data } = this.props;
        let { active } = this.state;
        return (
            <div className="tags">
                <ul>
                    {
                        data ? data.map((elem, index) => {
                            return <li onClick={e => this.clickTags(elem)} className={elem.name === active ? 'active' : ''} key={elem.id}>{elem.name}</li>;
                        }) : null
                    }
                </ul>
            </div>
        );
    }
}