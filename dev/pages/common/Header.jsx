import React, { Component } from 'react';
import { Link } from 'react-router';
import { DIST } from 'conf';

export default class Header extends Component {
    componentDidMount() {
        // ...
    }
    render() {
        let { style } = this.props;
        return (
            <div className="header">
                <Link to={DIST}>index</Link>
                <Link to={DIST + '/ReduxDom'}>redux</Link>
                <Link to={DIST + '/404'}>404</Link>
            </div>
        );
    }
}