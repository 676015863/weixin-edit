import React, { Component } from 'react';
import { DIST } from 'conf';
import { CirclePicker, SketchPicker } from 'react-color';

export default class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            color: '#000'
        }
        this.colors = ["#f44336", "#e91e63", "#9c27b0", "#673ab7", "#3f51b5", "#2196f3", "#03a9f4", "#00bcd4", "#009688", "#4caf50"];
    }

    showSketchPicker = e => {
        this.setState({ show: !this.state.show });
    }

    changeColor = color => {
        this.setState({
            color: color.hex
        });
        $(document).trigger('color', color.hex);
    }

    render() {
        let { show, color } = this.state;
        return (
            <div className="colors">
                <span className="colors-list">
                    <CirclePicker color={color} onChange={this.changeColor} circleSize={24} circleSpacing={10} width={340} colors={this.colors} />
                </span>
                <a className="more" onClick={this.showSketchPicker}>...</a>
                {
                    show ? <div className="more-colors">
                        <SketchPicker color={color} onChange={this.changeColor} />
                    </div> : null
                }
            </div>
        );
    }
}