import React, { Component } from 'react';
import { DIST } from 'conf';

export default class Index extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {
        let { top, bottom, zIndex, className } = this.props;
        let cName = ['listbox'];
        if (className) {
            cName.push(className);
        }
        return (
            <div className={cName.join(' ')} style={{ top: top, bottom: bottom === undefined ? 48 : bottom, zIndex: zIndex || 0 }}>
                {this.props.children}
            </div>
        );
    }
}