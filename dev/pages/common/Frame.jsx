import './common.scss';
import React, { Component } from 'react';
export default class Frame extends Component {
    render() {
        let cName = ['frame'];
        let { className, children} = this.props;
        if (className) {
            cName.push(className);
        }
        return (
            <div className={cName.join(' ')}>
                {children}
            </div>
        );
    }
}