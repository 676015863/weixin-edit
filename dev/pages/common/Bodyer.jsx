import React, { Component } from 'react';
import Footer from './Footer';
import Header from './Header';

export default class Bodyer extends Component {

    constructor(props) {
        super(props); // 
    }

    render() {
        let cName = ['frame'];
        let { className, children, hasHeader, hasFooter, headerFull } = this.props;
        if (className) {
            cName.push(className);
        }
        let style = null;
        if (headerFull) {
            style = { width: '100%', padding: '0 20px' };
        }
        return (
            <div className={cName.join(' ')}>
                {hasHeader === false ? null : <Header style={style} />}
                <div className="bodyer">
                    {children}
                </div>
                {hasFooter === false ? null : <Footer />}
            </div>
        );
    }
}