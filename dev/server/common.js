import ajax from '../utils/ajax';

/**
 * @desc 验证码
*/
export function imgCode(data) {
    return ajax({
        type: 'get',
        url: '/api/imgCode',
        data: data
    });
}

/**
 * @desc 文件上传
*/
export function upload(data) {
    return ajax({
        type: 'post',
        url: '/api/upload',
        data: data
    });
}

/**
 * @desc 不设限文件上传,ueditor 用
*/
export function ueup(data) {
    return ajax({
        type: 'post',
        url: '/api/ueup',
        data: data
    });
}