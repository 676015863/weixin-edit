import ajax from '../utils/ajax';

/**
 * @desc 获取图片素材
*/
export function getImgs(data) {
    return ajax({
        type: 'post',
        url: '/api/getImgs',
        data: data
    });
}

/**
 * @desc 保存图片
*/
export function saveImgs(data) {
    return ajax({
        type: 'post',
        url: '/api/saveImgs',
        data: data
    });
}

/**
 * @desc 删除图片
*/
export function delImgs(data) {
    return ajax({
        type: 'post',
        url: '/api/delImgs',
        data: data
    });
}

/**
 * @desc 获取模板
*/
export function getTpls(data) {
    return ajax({
        type: 'post',
        url: '/api/getTpls',
        data: data
    });
}

/**
 * @desc 删除模板
*/
export function delTpl(data) {
    return ajax({
        type: 'post',
        url: '/api/delTpl',
        data: data
    });
}

/**
 * @desc 保存模板
*/
export function saveTpl(data) {
    return ajax({
        type: 'post',
        url: '/api/saveTpl',
        data: data
    });
}

/**
 * @desc 修改模板
*/
export function updateTpl(data) {
    return ajax({
        type: 'post',
        url: '/api/updateTpl',
        data: data
    });
}

/**
 * @desc 获取分类
*/
export function getTypes(data) {
    return ajax({
        type: 'post',
        url: '/api/getTypes',
        data: data
    });
}