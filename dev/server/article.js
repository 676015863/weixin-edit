import ajax from '../utils/ajax';

/**
 * @desc 获取文章
*/
export function getArticle(data) {
    return ajax({
        type: 'post',
        url: '/api/getArticle',
        data: data
    });
}

/**
 * @desc 删除文章
*/
export function delArticle(data) {
    return ajax({
        type: 'post',
        url: '/api/delArticle',
        data: data
    });
}

/**
 * @desc 保存文章
*/
export function saveArticle(data) {
    return ajax({
        type: 'post',
        url: '/api/saveArticle',
        data: data
    });
}

/**
 * @desc 修改文章
*/
export function updateArticle(data) {
    return ajax({
        type: 'post',
        url: '/api/updateArticle',
        data: data
    });
}