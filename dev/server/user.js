import ajax from '../utils/ajax';

/**
 * @desc 登录接口
*/
export function login(data) {
    return ajax({
        type: 'post',
        url: '/api/login',
        data: data
    });
}

/**
 * @desc 登录接口
*/
export function register(data) {
    return ajax({
        type: 'post',
        url: '/api/register',
        data: data
    });
}

/**
 * @desc 获取用户信息
*/
export function getUser(data) {
    return ajax({
        type: 'post',
        url: '/api/getUser',
        data: data
    });
}

/**
 * @desc 获取用户列表
*/
export function getUserList(data) {
    return ajax({
        type: 'post',
        url: '/api/getUserList',
        data: data
    });
}

/**
 * @desc 重置密码
*/
export function resetPassword(data) {
    return ajax({
        type: 'post',
        url: '/api/resetPassword',
        data: data
    });
}

/**
 * @desc 删除用户
*/
export function delUser(data) {
    return ajax({
        type: 'post',
        url: '/api/delUser',
        data: data
    });
}