// conf/db.js
// 
function nonce() {
      for (var e = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"], t = 0; t < 500; t++)
          for (var n = "", i = 0; i < 9; i++) {
              var a = Math.floor(16 * Math.random());
              n += e[a]
          }
      return n
}
function xyz(e) {
    function t(e) {
        return i(n(a(e)))
    }
    function n(e) {
        return o(s(r(e), 8 * e.length))
    }
    function i(e) {
        try {} catch (t) {
            f = 0
        }
        for (var n, i = f ? "0123456789ABCDEF" : "0123456789abcdef", a = "", r = 0; r < e.length; r++)
            n = e.charCodeAt(r),
            a += i.charAt(n >>> 4 & 15) + i.charAt(15 & n);
        return a
    }
    function a(e) {
        for (var t, n, i = "", a = -1; ++a < e.length; )
            t = e.charCodeAt(a),
            n = a + 1 < e.length ? e.charCodeAt(a + 1) : 0,
            55296 <= t && t <= 56319 && 56320 <= n && n <= 57343 && (t = 65536 + ((1023 & t) << 10) + (1023 & n),
            a++),
            t <= 127 ? i += String.fromCharCode(t) : t <= 2047 ? i += String.fromCharCode(192 | t >>> 6 & 31, 128 | 63 & t) : t <= 65535 ? i += String.fromCharCode(224 | t >>> 12 & 15, 128 | t >>> 6 & 63, 128 | 63 & t) : t <= 2097151 && (i += String.fromCharCode(240 | t >>> 18 & 7, 128 | t >>> 12 & 63, 128 | t >>> 6 & 63, 128 | 63 & t));
        return i
    }
    function r(e) {
        for (var t = Array(e.length >> 2), n = 0; n < t.length; n++)
            t[n] = 0;
        for (var n = 0; n < 8 * e.length; n += 8)
            t[n >> 5] |= (255 & e.charCodeAt(n / 8)) << n % 32;
        return t
    }
    function o(e) {
        for (var t = "", n = 0; n < 32 * e.length; n += 8)
            t += String.fromCharCode(e[n >> 5] >>> n % 32 & 255);
        return t
    }
    function s(e, t) {
        e[t >> 5] |= 128 << t % 32,
        e[(t + 64 >>> 9 << 4) + 14] = t;
        for (var n = 1732584193, i = -271733879, a = -1732584194, r = 271733878, o = 0; o < e.length; o += 16) {
            var s = n
              , l = i
              , m = a
              , f = r;
            n = c(n, i, a, r, e[o + 0], 7, -680876936),
            r = c(r, n, i, a, e[o + 1], 12, -389564586),
            a = c(a, r, n, i, e[o + 2], 17, 606105819),
            i = c(i, a, r, n, e[o + 3], 22, -1044525330),
            n = c(n, i, a, r, e[o + 4], 7, -176418897),
            r = c(r, n, i, a, e[o + 5], 12, 1200080426),
            a = c(a, r, n, i, e[o + 6], 17, -1473231341),
            i = c(i, a, r, n, e[o + 7], 22, -45705983),
            n = c(n, i, a, r, e[o + 8], 7, 1770035416),
            r = c(r, n, i, a, e[o + 9], 12, -1958414417),
            a = c(a, r, n, i, e[o + 10], 17, -42063),
            i = c(i, a, r, n, e[o + 11], 22, -1990404162),
            n = c(n, i, a, r, e[o + 12], 7, 1804603682),
            r = c(r, n, i, a, e[o + 13], 12, -40341101),
            a = c(a, r, n, i, e[o + 14], 17, -1502002290),
            i = c(i, a, r, n, e[o + 15], 22, 1236535329),
            n = u(n, i, a, r, e[o + 1], 5, -165796510),
            r = u(r, n, i, a, e[o + 6], 9, -1069501632),
            a = u(a, r, n, i, e[o + 11], 14, 643717713),
            i = u(i, a, r, n, e[o + 0], 20, -373897302),
            n = u(n, i, a, r, e[o + 5], 5, -701558691),
            r = u(r, n, i, a, e[o + 10], 9, 38016083),
            a = u(a, r, n, i, e[o + 15], 14, -660478335),
            i = u(i, a, r, n, e[o + 4], 20, -405537848),
            n = u(n, i, a, r, e[o + 9], 5, 568446438),
            r = u(r, n, i, a, e[o + 14], 9, -1019803690),
            a = u(a, r, n, i, e[o + 3], 14, -187363961),
            i = u(i, a, r, n, e[o + 8], 20, 1163531501),
            n = u(n, i, a, r, e[o + 13], 5, -1444681467),
            r = u(r, n, i, a, e[o + 2], 9, -51403784),
            a = u(a, r, n, i, e[o + 7], 14, 1735328473),
            i = u(i, a, r, n, e[o + 12], 20, -1926607734),
            n = h(n, i, a, r, e[o + 5], 4, -378558),
            r = h(r, n, i, a, e[o + 8], 11, -2022574463),
            a = h(a, r, n, i, e[o + 11], 16, 1839030562),
            i = h(i, a, r, n, e[o + 14], 23, -35309556),
            n = h(n, i, a, r, e[o + 1], 4, -1530992060),
            r = h(r, n, i, a, e[o + 4], 11, 1272893353),
            a = h(a, r, n, i, e[o + 7], 16, -155497632),
            i = h(i, a, r, n, e[o + 10], 23, -1094730640),
            n = h(n, i, a, r, e[o + 13], 4, 681279174),
            r = h(r, n, i, a, e[o + 0], 11, -358537222),
            a = h(a, r, n, i, e[o + 3], 16, -722521979),
            i = h(i, a, r, n, e[o + 6], 23, 76029189),
            n = h(n, i, a, r, e[o + 9], 4, -640364487),
            r = h(r, n, i, a, e[o + 12], 11, -421815835),
            a = h(a, r, n, i, e[o + 15], 16, 530742520),
            i = h(i, a, r, n, e[o + 2], 23, -995338651),
            n = p(n, i, a, r, e[o + 0], 6, -198630844),
            r = p(r, n, i, a, e[o + 7], 10, 1126891415),
            a = p(a, r, n, i, e[o + 14], 15, -1416354905),
            i = p(i, a, r, n, e[o + 5], 21, -57434055),
            n = p(n, i, a, r, e[o + 12], 6, 1700485571),
            r = p(r, n, i, a, e[o + 3], 10, -1894986606),
            a = p(a, r, n, i, e[o + 10], 15, -1051523),
            i = p(i, a, r, n, e[o + 1], 21, -2054922799),
            n = p(n, i, a, r, e[o + 8], 6, 1873313359),
            r = p(r, n, i, a, e[o + 15], 10, -30611744),
            a = p(a, r, n, i, e[o + 6], 15, -1560198380),
            i = p(i, a, r, n, e[o + 13], 21, 1309151649),
            n = p(n, i, a, r, e[o + 4], 6, -145523070),
            r = p(r, n, i, a, e[o + 11], 10, -1120210379),
            a = p(a, r, n, i, e[o + 2], 15, 718787259),
            i = p(i, a, r, n, e[o + 9], 21, -343485551),
            n = d(n, s),
            i = d(i, l),
            a = d(a, m),
            r = d(r, f)
        }
        return Array(n, i, a, r)
    }
    function l(e, t, n, i, a, r) {
        return d(m(d(d(t, e), d(i, r)), a), n)
    }
    function c(e, t, n, i, a, r, o) {
        return l(t & n | ~t & i, e, t, a, r, o)
    }
    function u(e, t, n, i, a, r, o) {
        return l(t & i | n & ~i, e, t, a, r, o)
    }
    function h(e, t, n, i, a, r, o) {
        return l(t ^ n ^ i, e, t, a, r, o)
    }
    function p(e, t, n, i, a, r, o) {
        return l(n ^ (t | ~i), e, t, a, r, o)
    }
    function d(e, t) {
        var n = (65535 & e) + (65535 & t)
          , i = (e >> 16) + (t >> 16) + (n >> 16);
        return i << 16 | 65535 & n
    }
    function m(e, t) {
        return e << t | e >>> 32 - t
    }
    var f = 0;
    return t(e)
}
//xyz('/xdnphb/data/weixinuser/searchWeixinDataByCondition?AppKey=joker&filter=&hasDeal=false&keyName=wuxiaobopd&order=NRI&nonce=017bfbdc0')
//xyz = url?AppKey=joker& + 参数 + &nonce=017bfbdc0

function getxyz(url, data){
	var item = '';
	var noc = nonce();
	for(var key in data){
		if(typeof data[key] == 'string'){
			item+= '&'+key+'='+ data[key]
		}else{
			item+= '&'+key+'='+ JSON.stringify(data[key])
		}
	}
	item+= '&nonce='+noc;
	var str = url+'?AppKey=joker'+item
	return {
		nonce: noc,
		xyz: xyz(str)
	}
}

module.exports = {
    getxyz:getxyz
};
