﻿// 公用接口
var { spiderTpl } = require('./action/spiderTpl.js');
var { spiderArt } = require('./action/spiderArt.js');
var { upload, uploadue, uploadBase64 } = require('./action/upload.js');

// 素材接口
var { getImgs, delImgs, saveImgs } = require('./action/imgs.js'); // 系统图片
var { getTpls, delTpl, saveTpl, updateTpl } = require('./action/tpls.js'); // 系统模板

// 分类
var { getTypes } = require('./action/types.js');

// 路由
function router(app, express) {
    // 静态模块
    app.use('/assets', express.static('assets')); // 静态资源
    app.use('/upload', express.static('upload')); // 上传图片
    app.use('/article', express.static('article')); // 存放文章

    // 公用接口
    app.post('/api/upload', upload); // 上传图片

    app.get('/api/uploadue', uploadue); // 上传图片
    app.post('/api/uploadue', uploadue); // 上传图片
    app.post('/api/uploadBase64', uploadBase64); // base64 上传

    // 素材接口
    app.post('/api/getImgs', getImgs);
    app.post('/api/delImgs', delImgs);
    app.post('/api/saveImgs', saveImgs);
    app.post('/api/getTpls', getTpls);
    app.post('/api/delTpl', delTpl);
    app.post('/api/saveTpl', saveTpl);
    app.post('/api/updateTpl', updateTpl);

    // 分类
    app.post('/api/getTypes', getTypes);

    app.post('/api/spiderList', spiderTpl);
    app.post('/api/spiderArt', spiderArt);

    // url路由
    app.get('/', function (req, res) { res.sendfile('./index.html'); }); // 主页
    app.get('*', function (req, res) { res.sendfile('./index.html'); }); // 404

    return app;
}

module.exports = router;
// export default router;