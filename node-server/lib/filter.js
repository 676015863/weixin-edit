var { result } = require('./result');

// 登录过滤
exports.authorize = function (req, res, next) {
    // req.session.user = {
    //     id: 19,
    //     username: '',
    //     email: null,
    //     tel: '13551301693',
    //     usertype: 1
    // };
    // next();
    if (!req.session.user) {
        // 返回值
        result(req, res, {
            code: 403,
            msg: '没有登录',
            success: false
        });
    } else {
        next();
    }
};

// 后台过滤
exports.adminAuthorize = function (req, res, next) {
    if (req.session.user && req.session.user.usertype !== 0) {
        // 返回值
        result(req, res, {
            code: 403,
            msg: '没有权限',
            success: false
        });
    } else {
        next();
    }
};