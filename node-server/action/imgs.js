var { result } = require('../lib/result');
var { readSQL } = require('../sql/readSQL');
var { deleteSQL } = require('../sql/deleteSQL');
var { createSQL } = require('../sql/createSQL');
var Sequelize = require('sequelize');
var sd = require('silly-datetime');

/**
 * @desc 获取系统图片
*/
exports.getImgs = function (req, res) {

    // 获取系统图片，name = ''
    let obj = {
        name: {
            '$like': `%${req.body.name || ''}%`
        },
        type: req.body.type || '',
        del: 0
    };
    if (obj.type === '') {
        delete obj.type;
    }
    readSQL({
        req: req,
        where: obj,
        table: 'wx_imgs',
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true },
            name: { type: Sequelize.CHAR },
            url: { type: Sequelize.CHAR },
            date: { type: Sequelize.CHAR },
            size: { type: Sequelize.CHAR },
            type: { type: Sequelize.CHAR }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 删除系统图片，只是删除了数据库
*/
exports.delImgs = function (req, res) {

    // 获取系统图片，name = ''
    deleteSQL({
        req: req,
        where: {
            id: req.body.id
        },
        table: 'wx_imgs',
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 保存图片
*/
exports.saveImgs = function (req, res) {
    // 保存sql
    createSQL({
        obj: {
            name: req.body.name,
            url: req.body.url,
            size: req.body.size,
            type: req.body.type || 0,
            date: sd.format(new Date(), 'YYYY/MM/DD HH:mm:ss')
        },
        table: 'wx_imgs',
        sequeObj: {
            // id: { type: Sequelize.INTEGER, primaryKey: true },
            name: { type: Sequelize.CHAR },
            url: { type: Sequelize.CHAR },
            size: { type: Sequelize.CHAR },
            type: { type: Sequelize.INTEGER },
            date: { type: Sequelize.CHAR }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: null,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};