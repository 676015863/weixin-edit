var Sequelize = require('sequelize');
var { result } = require('../lib/result');
var { readSQL } = require('../sql/readSQL');
var { deleteSQL } = require('../sql/deleteSQL');
var { createSQL } = require('../sql/createSQL');
var { updateSQL } = require('../sql/updateSQL');
var { aesEncrypt } = require('../lib/md5');
var sd = require('silly-datetime');

/**
 * @desc 获取用户列表
*/
exports.getUserList = function (req, res) {

    readSQL({
        req: req,
        table: 'wx_user',
        where: {
            tel: {
                '$like': `%${req.body.tel || ''}%`
            }
        },
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true },
            username: { type: Sequelize.CHAR },
            usertype: { type: Sequelize.INTEGER },
            updateTime: { type: Sequelize.CHAR },
            tel: { type: Sequelize.CHAR },
            del: { type: Sequelize.INTEGER }
        },
        callBack: (ret) => {
            if (ret) {
                // console.log(ret);
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功!',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 重置密码 resetPassword
*/
exports.resetPassword = function (req, res) {
    updateSQL(
        'wx_user',
        {
            id: { type: Sequelize.INTEGER, primaryKey: true },
            password: { type: Sequelize.CHAR }
        },
        { password: aesEncrypt(req.body.password) },
        { id: req.body.id || req.session.user.id },
        (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功!',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        });
};

/**
 * @desc 删除用户
*/
exports.delUser = function (req, res) {
    deleteSQL({
        req: req,
        table: 'wx_user',
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true }
        },
        where: { id: req.body.id },
        callback: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功!',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};