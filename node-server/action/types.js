var { result } = require('../lib/result');
var { readSQL } = require('../sql/readSQL');
var { deleteSQL } = require('../sql/deleteSQL');
var { createSQL } = require('../sql/createSQL');
var { updateSQL } = require('../sql/updateSQL');
var Sequelize = require('sequelize');
var sd = require('silly-datetime');

/**
 * @desc 获取分类 0 是图片分类， 1 是模板分类
 */
exports.getTypes = function (req, res) {

    readSQL({
        req: req,
        table: 'wx_types',
        where: {
            type: req.body.type,
            del: 0
        },
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true },
            type: { type: Sequelize.CHAR },
            name: { type: Sequelize.CHAR }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};