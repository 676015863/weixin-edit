var { result } = require('../lib/result');
var { readSQL } = require('../sql/readSQL');
var { deleteSQL } = require('../sql/deleteSQL');
var { createSQL } = require('../sql/createSQL');
var { updateSQL } = require('../sql/updateSQL');
var Sequelize = require('sequelize');
var sd = require('silly-datetime');

/**
 * @desc 获取文章
*/
exports.getArticle = function (req, res) {

    // 获取系统图片，name = ''
    let obj = {
        name: {
            '$like': `%${req.body.name || ''}%`
        },
        owner: req.session.user.id
    };
    if (obj.type === '') {
        delete obj.type;
    }
    readSQL({
        req: req,
        where: obj,
        table: 'wx_article',
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true },
            name: { type: Sequelize.CHAR },
            owner: { type: Sequelize.CHAR },
            author: { type: Sequelize.CHAR },
            pic: { type: Sequelize.CHAR },
            date: { type: Sequelize.CHAR },
            data: { type: Sequelize.TEXT('long') },
            des: { type: Sequelize.CHAR }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 删除模板
*/
exports.delArticle = function (req, res) {

    // 获取系统图片，name = ''
    deleteSQL({
        req: req,
        where: {
            id: req.body.id,
            owner: req.session.user.id
        },
        table: 'wx_article',
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true },
            owner: { type: Sequelize.CHAR }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 保存tpl
*/
exports.saveArticle = function (req, res) {
    // 保存sql
    createSQL({
        obj: {
            name: req.body.name,
            pic: req.body.pic,
            data: req.body.data || '',
            author: req.body.author || '',
            owner: req.session.user.id,
            des: req.body.des || '',
            date: sd.format(new Date(), 'YYYY/MM/DD HH:mm:ss')
        },
        table: 'wx_article',
        sequeObj: {
            // id: { type: Sequelize.INTEGER, primaryKey: true },
            name: { type: Sequelize.CHAR },
            pic: { type: Sequelize.CHAR },
            author: { type: Sequelize.CHAR },
            owner: { type: Sequelize.CHAR },
            data: { type: Sequelize.TEXT('long') },
            des: { type: Sequelize.CHAR },
            date: { type: Sequelize.CHAR }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: null,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 修改数据
*/
exports.updateArticle = function (req, res) {
    // 保存sql
    updateSQL('wx_article', {
        id: { type: Sequelize.INTEGER, primaryKey: true },
        name: { type: Sequelize.CHAR },
        pic: { type: Sequelize.CHAR },
        author: { type: Sequelize.CHAR },
        owner: { type: Sequelize.CHAR },
        data: { type: Sequelize.TEXT('long') },
        des: { type: Sequelize.CHAR },
        date: { type: Sequelize.CHAR }
    },
        {
            name: req.body.name,
            pic: req.body.pic,
            data: req.body.data,
            author: req.body.author,
            des: req.body.des || '',
            date: sd.format(new Date(), 'YYYY/MM/DD HH:mm:ss')
        }, { id: req.body.id, owner: req.session.user.id }, (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        });
};