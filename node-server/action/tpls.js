var { result } = require('../lib/result');
var { readSQL } = require('../sql/readSQL');
var { deleteSQL } = require('../sql/deleteSQL');
var { createSQL } = require('../sql/createSQL');
var { updateSQL } = require('../sql/updateSQL');
var Sequelize = require('sequelize');
var sd = require('silly-datetime');

/**
 * @desc 获取模板
*/
exports.getTpls = function (req, res) {

    // 获取系统图片，name = ''
    let obj = {
        name: {
            '$like': `%${req.body.name || ''}%`
        },
        type: req.body.type || '',
        del: 0
    };
    if (obj.type === '') {
        delete obj.type;
    }
    readSQL({
        req: req,
        where: obj,
        table: 'wx_tpls',
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true },
            name: { type: Sequelize.CHAR },
            pic: { type: Sequelize.CHAR },
            date: { type: Sequelize.CHAR },
            data: { type: Sequelize.TEXT('long') },
            des: { type: Sequelize.CHAR },
            type: { type: Sequelize.CHAR }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 删除模板
*/
exports.delTpl = function (req, res) {

    // 获取系统图片，name = ''
    deleteSQL({
        req: req,
        where: {
            id: req.body.id
        },
        table: 'wx_tpls',
        sequeObj: {
            id: { type: Sequelize.INTEGER, primaryKey: true }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret.rows,
                    count: ret.count,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 保存tpl
*/
exports.saveTpl = function (req, res) {
    // 保存sql
    createSQL({
        obj: {
            name: req.body.name,
            pic: req.body.pic,
            data: req.body.data,
            type: req.body.type || 0,
            des: req.body.des || '',
            date: sd.format(new Date(), 'YYYY/MM/DD HH:mm:ss')
        },
        table: 'wx_tpls',
        sequeObj: {
            // id: { type: Sequelize.INTEGER, primaryKey: true },
            name: { type: Sequelize.CHAR },
            pic: { type: Sequelize.CHAR },
            data: { type: Sequelize.TEXT('long') },
            type: { type: Sequelize.INTEGER },
            des: { type: Sequelize.CHAR },
            date: { type: Sequelize.CHAR }
        },
        callBack: (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: null,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        }
    });
};

/**
 * @desc 修改数据
*/
exports.updateTpl = function (req, res) {
    // 保存sql
    updateSQL('wx_tpls', {
        id: { type: Sequelize.INTEGER, primaryKey: true },
        name: { type: Sequelize.CHAR },
        pic: { type: Sequelize.CHAR },
        data: { type: Sequelize.TEXT('long') },
        type: { type: Sequelize.INTEGER },
        des: { type: Sequelize.CHAR },
        date: { type: Sequelize.CHAR }
    },
        {
            name: req.body.name,
            pic: req.body.pic,
            data: req.body.data,
            type: req.body.type || 0,
            des: req.body.des || '',
            date: sd.format(new Date(), 'YYYY/MM/DD HH:mm:ss')
        }, { id: req.body.id }, (ret) => {
            if (ret) {
                result(req, res, {
                    code: 200,
                    data: ret,
                    msg: '成功',
                    success: true
                });
            } else {
                // 返回值
                result(req, res, {
                    code: 500,
                    data: ret,
                    msg: '失败',
                    success: false
                });
            }
        });
};