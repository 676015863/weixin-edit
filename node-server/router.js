﻿

// 公用接口
var { upload, uploadue, uploadBase64 } = require('./action/upload.js');
var { imgCode } = require('./action/imgCode.js');

// 用户操作
var { getUserList, resetPassword, delUser } = require('./action/user.js');
var { login, getUser, logout } = require('./action/login.js');
var { register } = require('./action/register.js');

// 过滤器
var { authorize, adminAuthorize } = require('./lib/filter');
var { limitReq } = require('./lib/limitRequest');

// 素材接口
var { getImgs, delImgs, saveImgs } = require('./action/imgs.js'); // 系统图片
var { getTpls, delTpl, saveTpl, updateTpl } = require('./action/tpls.js'); // 系统模板

// 分类
var { getTypes } = require('./action/types.js');

// 文章
var { getArticle, delArticle, saveArticle, updateArticle } = require('./action/article'); // 用户H5 xx

// 路由
function router(app, express) {
    // 静态模块
    app.use('/assets', express.static('assets')); // 静态资源
    app.use('/upload', express.static('upload')); // 上传图片
    app.use('/article', express.static('article')); // 存放文章

    // 公用接口
    app.get('/api/imgCode', imgCode); // 验证码
    app.post('/api/upload', upload); // 上传图片

    app.get('/api/uploadue', uploadue); // 上传图片
    app.post('/api/uploadue', uploadue); // 上传图片
    
    app.post('/api/uploadBase64', authorize, uploadBase64); // base64 上传

    // 用户
    app.get('/api/logout', logout); // 退出
    app.post('/api/login', limitReq, login); // 登录
    app.post('/api/register', limitReq, register); // 登录
    app.post('/api/getUser', authorize, getUser); // 获取用户信息
    app.post('/api/getUserList', authorize, adminAuthorize, getUserList); // 获取用户列表
    app.post('/api/resetPassword', authorize, adminAuthorize, resetPassword); // 重置密码
    app.post('/api/delUser', authorize, delUser); // 禁用账号

    // 素材接口
    app.post('/api/getImgs', getImgs);
    app.post('/api/delImgs', authorize, adminAuthorize, delImgs);
    app.post('/api/saveImgs', authorize, adminAuthorize, saveImgs);
    app.post('/api/getTpls', getTpls);
    app.post('/api/delTpl', authorize, adminAuthorize, delTpl);
    app.post('/api/saveTpl', authorize, adminAuthorize, saveTpl);
    app.post('/api/updateTpl', authorize, adminAuthorize, updateTpl);

    // 分类
    app.post('/api/getTypes', getTypes);

    // 文章
    app.post('/api/getArticle', authorize, getArticle);
    app.post('/api/delArticle', authorize, delArticle);
    app.post('/api/saveArticle', authorize, saveArticle);
    app.post('/api/updateArticle', authorize, updateArticle);

    // url路由
    app.get('/', function (req, res) { res.sendfile('./index.html'); }); // 主页
    app.get('*', function (req, res) { res.sendfile('./index.html'); }); // 404

    return app;
}

module.exports = router;
// export default router;