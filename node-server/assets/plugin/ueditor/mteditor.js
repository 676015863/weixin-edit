$(function () {
    // 自定义的普通按钮
    UE.registerUI('mt', function (editor, uiName) {

        // 当点到编辑内容上时，按钮要做的状态反射
        editor.addListener('ready', function () {
            // console.log(editor.body);
            $(editor.body).on('click', '.RankEditor', function () {
                // console.log($(this)[0]);
                $(document).trigger('clickRank', this);
            });
        });

    });
});